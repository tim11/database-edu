from flask import Blueprint
from .modules import b_regions

backend = Blueprint(
    'backend',
    __name__,
    template_folder='./views',
    static_folder='./assets/node_modules'
)

backend.register_blueprint(
    b_regions,
    url_prefix='/regions'
)

__all__ = ['backend']
