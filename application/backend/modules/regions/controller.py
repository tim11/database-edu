from flask import Blueprint, render_template, redirect, abort, url_for
from common.services import DatabaseStorate as Storage
from .forms import CreateUpdate

regions = Blueprint(
    'regions',
    __name__,
    template_folder='../../views/modules/regions'
)


@regions.route('/', methods=['GET'])
def index():
    items = Storage.regions.all()
    return render_template('index.html', title=' Регионы', items=items)


@regions.route('/add', methods=['GET', 'POST'])
def create():
    form = CreateUpdate(Storage)
    if form.save():
        return redirect(url_for('backend.regions.index'))
    return render_template('form.html', title='Добавление региона', form=form)


@regions.route('/edit/<int:id>', methods=['GET', 'POST'])
def update(id):
    item = Storage.regions.find_by_id(id)
    if not item:
        abort(404, message='Запись с данным ID не найдена')
    form = CreateUpdate(Storage, data=item.to_dict)
    if form.save():
        return redirect(url_for('backend.regions.index'))
    return render_template('form.html', title=' Изменение региона', form=form)


@regions.route('/delete/<int:id>', methods=['GET'])
def delete(id):
    item = Storage.regions.find_by_id(id)
    if not item:
        abort(404, message='Запись с данным ID не найдена')
    Storage.regions.delete(item)
    return redirect(url_for('backend.regions.index'))

