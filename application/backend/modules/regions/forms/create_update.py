from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, DateTimeField
from wtforms.validators import DataRequired
from datetime import datetime
from common.domains import Region


class CreateUpdate(FlaskForm):
    id = IntegerField(label='Идентификатор', default=0)
    name = StringField(label='Название региона', validators=[DataRequired()])
    created_at = DateTimeField(label='Дата создания', default=datetime.now())
    updated_at = DateTimeField(label='Дата измения', default=datetime.now())

    def __init__(self, storage, *args, **kwargs):
        self.__storage = storage
        super().__init__(*args, **kwargs)

    def save(self) -> bool:
        if not self.validate_on_submit():
            return False
        item = Region(
            id=self.id.data,
            name=self.name.data,
            created_at=self.created_at.data,
            updated_at=self.updated_at.data
        )
        errors = self.__storage.regions.save(item)
        if not errors:
            return True
        for key, error in errors.items():
            self.__getattribute__(key).errors.append(error)
        return False
