from dataclasses import dataclass
from datetime import datetime


@dataclass
class Region:
    id: int
    name: str
    created_at: datetime = datetime.now()
    updated_at: datetime = datetime.now()

    @property
    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'created_at': self.created_at,
            'updated_at': self.updated_at
        }

