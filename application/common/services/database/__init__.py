from .Regions import Regions
from .connection import get_db_connection


class DatabaseStorage:
    def __init__(self):
        self.__regions = Regions(get_db_connection())

    @property
    def regions(self) -> Regions:
        return self.__regions


storage = DatabaseStorage()


__all__ = ['DatabaseStorage', 'storage']
