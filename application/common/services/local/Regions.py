from typing import List, Dict
from datetime import datetime
from common.domains import Region


class Regions:
    def __init__(self):
        self.__data = {
            1: {
                'id': 1,
                'name': 'Башкортостан',
                'created_at': 1679700554,
                'updated_at': 1679700554
            },
            2: {
                'id': 2,
                'name': 'Татарстан',
                'created_at': 1679700554,
                'updated_at': 1679700554
            }
        }

    @staticmethod
    def __map(record: Dict) -> Region:
        return Region(
            id=record.get('id'),
            name=record.get('name'),
            created_at=datetime.fromtimestamp(record.get('created_at')),
            updated_at=datetime.fromtimestamp(record.get('updated_at'))
        )

    def all(self) -> List[Region] | List:
        data = list()
        for item in self.__data.values():
            data.append(self.__map(item))
        return data

    def find_by_id(self, id: int) -> Region | None:
        item = self.__data.get(id)
        if not item:
            return
        return self.__map(item)

    def save(self, item: Region) -> Dict | None:
        name = item.name
        id = item.id
        errors = dict()
        print(item)
        if not id:
            last_id = 0
            if self.__data:
                last_id = max(self.__data.keys())
            id = last_id + 1
            check = list(filter(lambda record: record.get('name') == name, self.__data.values()))
            if check:
                errors['name'] = 'Запись с таким именем уже существует'
        elif id not in self.__data:
            errors['id'] = 'Запись с данным ID не найдена'
        else:
            check = list(filter(lambda record: record.get('name') == name and record.get('id') != id,
                                self.__data.values()))
            if check:
                errors['name'] = 'Запись с таким именем уже существует'
            item.updated_at = datetime.now()
        if errors:
            return errors
        self.__data[id] = {
            'id': id,
            'name': name,
            'created_at': int(datetime.timestamp(item.created_at)),
            'updated_at': int(datetime.timestamp(item.updated_at))
        }

    def delete(self, item: Region):
        if item.id in self.__data:
            del self.__data[item.id]
