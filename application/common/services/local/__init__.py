from .Regions import Regions


class LocalStorage:
    def __init__(self):
        self.__regions = Regions()

    @property
    def regions(self) -> Regions:
        return self.__regions


storage = LocalStorage()


__all__ = ['storage']
