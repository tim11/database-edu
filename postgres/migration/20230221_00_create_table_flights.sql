CREATE TABLE IF NOT EXISTS flights (
    flight_id SERIAL NOT NULL,
    flight_no CHAR(6) NOT NULL,
    scheduled_departure timestamptz NOT NULL,
    scheduled_arrival timestamptz NOT NULL,
    departure_airport CHAR(3) NOT NULL,
    arrival_airport CHAR(3) NOT NULL,
    status VARCHAR(20) NOT NULL,
    aircraft_code CHAR(3) NOT NULL,
    actual_departure timestamptz,
    actual_arrival timestamptz,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (flight_id),
    UNIQUE (flight_no, scheduled_departure),
    CHECK (scheduled_departure > scheduled_arrival),
    CHECK (
        actual_arrival IS NULL OR  
        (
            actual_departure IS NOT NULL AND
            actual_arrival > actual_departure
        )
    ),
    CHECK (
        status IN (
            'On Time', 'Delayed', 'Departed',
            'Arrived', 'Scheduled', 'Cancelled'
        )
    ),
    FOREIGN KEY (aircraft_code) REFERENCES aircrafts(aircraft_code),
    FOREIGN KEY (arrival_airport) REFERENCES airports(airport_code),
    FOREIGN KEY (departure_airport) REFERENCES airports(airport_code)
);


COMMENT ON COLUMN flights.flight_id IS 'Идентификатор рейса';
COMMENT ON COLUMN flights.flight_no IS 'Номер рейса';
COMMENT ON COLUMN flights.scheduled_departure IS 'Время вылета по расписанию';
COMMENT ON COLUMN flights.scheduled_arrival IS 'Время прилёта по расписанию';
COMMENT ON COLUMN flights.departure_airport IS 'Аэропорт отправления';
COMMENT ON COLUMN flights.arrival_airport IS 'Аэропорт прибытия';
COMMENT ON COLUMN flights.aircraft_code IS ' Код самолета, IATA';
COMMENT ON COLUMN flights.status IS 'Статус рейса';
COMMENT ON COLUMN flights.actual_departure IS 'Фактическое время вылета';
COMMENT ON COLUMN flights.actual_arrival IS 'Фактическое время прилёта';
COMMENT ON COLUMN flights.created_at IS 'Дата создания';
COMMENT ON COLUMN flights.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON flights;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON flights
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
