CREATE TABLE IF NOT EXISTS bookings (
    book_ref CHAR(6) NOT NULL,
    book_date timestamptz NOT NULL,
    total_amount NUMERIC(10, 2) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (book_ref)
);


COMMENT ON COLUMN bookings.book_ref IS 'Номер бронирования';
COMMENT ON COLUMN bookings.book_date IS 'Дата бронирования';
COMMENT ON COLUMN bookings.total_amount IS 'Полная сумма бронирования';
COMMENT ON COLUMN bookings.created_at IS 'Дата создания';
COMMENT ON COLUMN bookings.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON bookings;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON bookings
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
