CREATE TABLE IF NOT EXISTS passengers (
    vat_id VARCHAR(20) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (vat_id)
);


COMMENT ON COLUMN passengers.vat_id IS 'Идентификатор пасажира';
COMMENT ON COLUMN passengers.created_at IS 'Дата создания';
COMMENT ON COLUMN passengers.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON passengers;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON passengers
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
