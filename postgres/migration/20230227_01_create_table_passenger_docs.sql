CREATE TABLE IF NOT EXISTS passenger_docs (
    id SERIAL NOT NULL,
    passenger_id VARCHAR(20) NOT NULL,
    p_serial CHAR(4) NOT NULL,
    p_number CHAR(6) NOT NULL,
    lastname TEXT NOT NULL,
    firstname TEXT NOT NULL,
    surname TEXT,
    birthday timestamptz NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id),
    UNIQUE(p_serial, p_number),
    FOREIGN KEY (passenger_id) REFERENCES passengers(vat_id) ON DELETE CASCADE
);


COMMENT ON COLUMN passenger_docs.id IS 'Идентификатор документа';
COMMENT ON COLUMN passenger_docs.id IS 'Идентификатор пасажира';
COMMENT ON COLUMN passenger_docs.p_serial IS 'Серия документа';
COMMENT ON COLUMN passenger_docs.p_number IS 'Номер документа';
COMMENT ON COLUMN passenger_docs.lastname IS 'Фамилия в документе';
COMMENT ON COLUMN passenger_docs.firstname IS 'Имя в документе';
COMMENT ON COLUMN passenger_docs.surname IS 'Отчество в документе';
COMMENT ON COLUMN passenger_docs.birthday IS 'Дата рождения в документе';
COMMENT ON COLUMN passenger_docs.created_at IS 'Дата создания';
COMMENT ON COLUMN passenger_docs.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON passenger_docs;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON passenger_docs
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
