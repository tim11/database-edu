CREATE TABLE IF NOT EXISTS tickets (
    ticket_no CHAR(13) NOT NULL,
    book_ref CHAR(6) NOT NULL,
    passenger_id VARCHAR(20) NOT NULL,
    passenger_name TEXT NOT NULL,
    contact_data jsonb,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (ticket_no),
    FOREIGN KEY (book_ref) REFERENCES bookings(book_ref),
    FOREIGN KEY (passenger_id) REFERENCES passengers(vat_id) ON UPDATE RESTRICT ON DELETE RESTRICT
);


COMMENT ON COLUMN tickets.ticket_no IS 'Номер билета';
COMMENT ON COLUMN tickets.book_ref IS 'Номер бронирования';
COMMENT ON COLUMN tickets.passenger_id IS 'Идентификатор пассажира';
COMMENT ON COLUMN tickets.passenger_name IS 'Имя пассажира';
COMMENT ON COLUMN tickets.contact_data IS 'Контактные данные пассажира';
COMMENT ON COLUMN tickets.created_at IS 'Дата создания';
COMMENT ON COLUMN tickets.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON tickets;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON tickets
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
