CREATE TABLE IF NOT EXISTS ticket_flights (
    ticket_no CHAR(13) NOT NULL,
    flight_id INTEGER NOT NULL,
    fare_conditions INTEGER NOT NULL,
    amount numeric(10, 2) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (ticket_no, flight_id),
    FOREIGN KEY (ticket_no) REFERENCES tickets(ticket_no),
    FOREIGN KEY (flight_id) REFERENCES flights(flight_id),
    FOREIGN KEY (fare_conditions) REFERENCES classes(id)
);


COMMENT ON COLUMN ticket_flights.ticket_no IS 'Номер билета';
COMMENT ON COLUMN ticket_flights.flight_id IS 'Идентификатор рейса';
COMMENT ON COLUMN ticket_flights.fare_conditions IS 'Класс обслуживания';
COMMENT ON COLUMN ticket_flights.amount IS 'Стоимость перелета';
COMMENT ON COLUMN ticket_flights.created_at IS 'Дата создания';
COMMENT ON COLUMN ticket_flights.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON ticket_flights;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON ticket_flights
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
