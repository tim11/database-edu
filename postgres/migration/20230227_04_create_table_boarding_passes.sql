CREATE TABLE IF NOT EXISTS boarding_passes (
    ticket_no CHAR(13) NOT NULL,
    flight_id INTEGER NOT NULL,
    boarding_no INTEGER NOT NULL,
    seat_no CHAR(4) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (ticket_no, flight_id),
    UNIQUE (flight_id, boarding_no),
    UNIQUE (flight_id, seat_no),
    FOREIGN KEY (ticket_no, flight_id) REFERENCES ticket_flights(ticket_no, flight_id)
);


COMMENT ON COLUMN boarding_passes.ticket_no IS 'Номер билета';
COMMENT ON COLUMN boarding_passes.flight_id IS 'Идентификатор рейса';
COMMENT ON COLUMN boarding_passes.boarding_no IS 'Номер посадочного талона';
COMMENT ON COLUMN boarding_passes.seat_no IS 'Номер места';
COMMENT ON COLUMN boarding_passes.created_at IS 'Дата создания';
COMMENT ON COLUMN boarding_passes.updated_at IS 'Дата изменения';

DROP TRIGGER IF EXISTS set_timestamp ON boarding_passes;
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON boarding_passes
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
